'use strict';
~ function() {
    var $ = TweenMax,
    ad = document.getElementById('mainContent'),
    mainImg = document.getElementById('mainImg'),
    
    tl = new TimelineMax(),
    //Random Numbers for set tiles random position
    
    //Total tiles
    count = 40,
    //Total columns and rows
    columns = 8,
    rows = 5,
    //Total width and height
    boxWidth = 50,
    boxHeight = 50,
    //1 to 40 Random Number
    randomTileNo = [],
    
    currentItemInMatrix = 0;

    window.init = function() {

    	for (var i = 0; i < count; i++) {

            // Creating Div Element for total tile and adding properties

    		var imgBox = document.createElement('div'),
    		    randomNo = Math.floor(Math.random() * 500) + (-200),
    		    imgBoxId;
    		
    		//adding box class then it will take properties from css
            imgBox.className = 'box';
    		imgBox.id = 'box'+(i + 1);
    		mainImg.appendChild(imgBox);
    		//imgBox.innerHTML = " " + (i + 1);
    		imgBoxId = document.getElementById('box'+(i + 1))

            // Setting background image in every tile
            imgBox.style.backgroundPositionX = -i * boxWidth + "px ";
            imgBox.style.backgroundPositionY =   "0px ";

            imgBox.style.top = 0;
            imgBox.style.left = i * boxWidth + "px";

            // move to next row
            if(i>(columns-1)){

            	if(i % columns === 0){
    				currentItemInMatrix++;
    			}

                imgBox.style.top = currentItemInMatrix * boxHeight + "px";
                imgBox.style.left = (i-columns*currentItemInMatrix) * boxWidth + "px";

    			imgBox.style.backgroundPositionX = -i * boxWidth + "px ";
    			imgBox.style.backgroundPositionY = -1 * currentItemInMatrix * boxHeight + "px";
            }

            //Placing div on random position
            tl.set(imgBox,{x:randomNo, y:randomNo, rotation:randomNo, ease:Sine.easeOut})

            //Adding Mouse over event
            imgBox.addEventListener("mouseover", setTileOnPos);

    	}

        function setTileOnPos(event){
            var cDiv = event.currentTarget.id,
                cDiv1 = document.getElementById(cDiv);

                cDiv1.style.zIndex = "-1";
                tl.to(cDiv1,.1,{x:0, y:0, rotation:0, ease:Sine.easeOut});
        }

        
        function playAnim(){
            console.log("hey")
            tl.staggerTo('.box',.5,{x:0, y:0, rotation:0, ease:Sine.easeOut}, 0.05, 1);
        }

    }

}();
